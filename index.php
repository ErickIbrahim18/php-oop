?<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("Shaun");

    echo "Animal Name : $sheep->name <br>";
    echo "Legs :$sheep->legs <br>";
    echo "Cold Blood : $sheep->cold_blooded <br><br>";

    $sungokong = new Ape("Kera sakti");
    echo "Animal Name : $sungokong->name <br>";
    echo "Legs : $sungokong->legs <br>";
    echo "Cold Blood : $sungokong->cold_blooded <br> ";

    $sungokong->yell();
    echo "<br>";
    echo "<br>";

    $kodok = new Frog("Buduk");
    echo "Animal Name : $kodok->name <br>";
    echo "Legs : $kodok->legs <br>";
    echo "Cold Blood : $kodok->cold_blooded <br>";
    $kodok->jump();
    ?>